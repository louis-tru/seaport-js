import { Seaport } from "./seaport";
import { ItemType, MAX_INT } from "./constants";
import { providers, VoidSigner, BigNumberish } from "ethers";
import { Bytes, BytesLike } from "@ethersproject/bytes";
import {
  Deferrable,
  defineReadOnly,
  resolveProperties,
  shallowCopy,
} from "@ethersproject/properties";
import {
  Provider,
  TransactionRequest,
  TransactionResponse,
} from "@ethersproject/abstract-provider";
import { ApprovalAction, CreateOrderAction } from "./types";

type TypedDataDomain = {
  name?: string;
  version?: string;
  chainId?: BigNumberish;
  verifyingContract?: string;
  salt?: BytesLike;
};

type TypedDataField = {
  name: string;
  type: string;
};

class MySigner extends VoidSigner {
  async _signTypedData(
    _domain: TypedDataDomain,
    _types: Record<string, Array<TypedDataField>>,
    _value: Record<string, any>
  ): Promise<string> {
    return "0x9b75e24e4419b772742c1317ac97e857668f028b3075e8b576beaf1c8af6bb75323ed2bf8462829d2b9f39ad2dc9061d3134ea6ee02cb0ed116a48062909c69e1c";
  }
}

export async function test() {
  debugger;
  // 	'X-API-KEY': '2f6f419a083c46de9d83ce3dbe7db601',
  // 	'X-API-ID': 'opensea-web',

  const accountAddress = "0x45d9dB730bac2A2515f107A3c75295E3504faFF7";
  const provider = new providers.JsonRpcProvider(
    "https://rinkeby.infura.io/v3/1ad04dd9dfa3421ca28519a575c959d9"
  );
  const signer = new MySigner(accountAddress, provider);
  const sea = new Seaport(signer);

  const startTime = "0";
  const endTime = MAX_INT.toString();
  const token = "0x7200CC8180DE111306D8A99E254381080dA48Fd7";
  const nftId = "1";

  let { actions } = await sea.createOrder(
    {
      startTime,
      endTime,
      offer: [
        {
          itemType: ItemType.ERC721,
          token: token,
          identifier: nftId,
        },
      ],
      consideration: [
        {
          amount: (1e16).toString(), // 0.01 eth
          recipient: accountAddress,
        },
      ],
      zone: "0x00000000e88fe2628ebc5da81d2b3cead633e89e",
    },
    accountAddress
  );

  // const approvalAction = actions[0] as ApprovalAction;
  // await approvalAction.transactionMethods.transact();

  const createOrderAction = actions[1] as CreateOrderAction;
  const order = await createOrderAction.createOrder();

  debugger;
}

test();
